import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './components/routers/inicio/inicio.component';
import { PedidosComponent } from './components/routers/pedidos/pedidos.component';
import { HamburguesaComponent } from './components/routers/productos/hamburguesa/hamburguesa.component';
import { NuevosComponent } from './components/routers/productos/nuevos/nuevos.component';

const routes: Routes = [
  {
    path: 'inicio',
    component: InicioComponent
  },
  {
    path: 'pedidos',
    component: PedidosComponent
  },
  {
    path: 'hamburguesa',
    component: HamburguesaComponent
  },
  {
    path: 'nuevos',
    component: NuevosComponent
  },
  {
    path:'**',
    component: InicioComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { PedidosComponent } from './pedidos/pedidos.component';
import { ProductosComponent } from './productos/productos.component';
import { HamburguesaComponent } from './productos/hamburguesa/hamburguesa.component';
import { NuevosComponent } from './productos/nuevos/nuevos.component';



@NgModule({
  declarations: [
    InicioComponent,
    PedidosComponent,
    ProductosComponent,
    HamburguesaComponent,
    NuevosComponent
  ],
  imports: [
    CommonModule
  ]
})
export class RoutersModule { }
